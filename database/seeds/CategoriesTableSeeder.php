<?php

use App\Category;
use Illuminate\Database\Seeder;

class CategoriesTableSeeder extends Seeder
{
    public function run()
    {
        $categories = [
            [
                'id'    => 1,
                'title' => 'Category A',
            ],
            [
                'id'    => 2,
                'title' => 'Category B',
            ],
        ];

        Category::insert($categories);
    }
}
