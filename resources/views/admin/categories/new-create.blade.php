@extends('layouts.admin')
@section('content')

<div class="card">
    <div class="card-header">
      {{ trans('cruds.category.title_singular') }}
    </div>

    <div class="card-body">

        <div class="form-group">
            <label for="parent_id">Parent Category</label>
            <select class="form-control select2 {{ $errors->has('parent') ? 'is-invalid' : '' }}" name="parent_id" id="parent_id">
                @foreach($parents as $id => $entry)
                <option value="{{ $entry->id }}" {{ old('parent_id') == $id ? 'selected' : '' }}>{{ $entry->title }}</option>
                @endforeach
            </select>
        </div>

        <div class="form-group">
            <label for="child_category">Child Category</label>
            <select name="child_category_id" id="child_category" class="form-control select2">
                <option value="">{{ trans('global.pleaseSelect') }}</option>
            </select>
        </div>

        <div class="form-group">
            <label for="child_category">Child Category Level</label>
            <select name="child_category_id" id="child_category_level" class="form-control select2">
                <option value="">{{ trans('global.pleaseSelect') }}</option>
            </select>
        </div>

    </div>
</div>


@section('scripts')
<script type="text/javascript">
    $("#parent_id").change(function() {
        $.ajax({
            url: "{{ route('admin.categories.getByParent') }}?parent_id=" + $(this).val(),
            method: 'GET',
            success: function(data) {
                $('#child_category').html(data.html);
            }
        });
    });
</script>
<script type="text/javascript">
    $("#child_category").change(function() {
        $.ajax({
            url: "{{ route('admin.categories.multiLevel') }}?child_id=" + $(this).val(),
            method: 'GET',
            success: function(data) {
                $('#child_category_level').html(data.html);
            }
        });
    });
</script>
@endsection

@endsection
