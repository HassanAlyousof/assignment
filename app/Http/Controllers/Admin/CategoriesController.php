<?php

namespace App\Http\Controllers\Admin;

use App\Category;
use App\Http\Controllers\Controller;
use Gate;
use Illuminate\Http\Request;
use Symfony\Component\HttpFoundation\Response;

class CategoriesController extends Controller
{
    public function index()
    {
        abort_if(Gate::denies('category_create'), Response::HTTP_FORBIDDEN, '403 Forbidden');

        $parents = Category::whereNull('parent_id')->get();

        return view('admin.categories.new-create', compact('parents'));
    }

    public function getByParent(Request $request)
    {
        abort_if(Gate::denies('category_create'), Response::HTTP_FORBIDDEN, '403 Forbidden');

        if (!$request->parent_id) {
            $html = '<option value="">'.trans('global.pleaseSelect').'</option>';
        } else {
            $html = '';

            $parent = Category::where('id', $request->parent_id)->first();

            if (strpos($parent->title, 'B') !== false) {
                $temp = 'B';
            } else {
                $temp = 'A';
            }

            for ($i=1; $i <= 2; $i++) {
                Category::create([
              'title' => 'SUB '.$temp.$i,
              'parent_id' => $request->parent_id,
            ]);
            }

            $child_categories = Category::where('parent_id', $request->parent_id)->get();

            foreach ($child_categories as $child_category) {
                $html .= '<option value="'.$child_category->id.'">'.$child_category->title.'</option>';
            }
        }

        return response()->json(['html' => $html]);
    }

    public function multiLevel(Request $request)
    {
        abort_if(Gate::denies('category_create'), Response::HTTP_FORBIDDEN, '403 Forbidden');

        if (!$request->child_id) {
            $html = '<option value="">'.trans('global.pleaseSelect').'</option>';
        } else {
            $html = '';

            $parent = Category::where('id', $request->child_id)->first();

            if (strpos($parent->title, 'B') !== false) {
                $temp = 'B';
            } else {
                $temp = 'A';
            }

            for ($i=1; $i <= 2; $i++) {
                Category::create([
              'title' => 'SUB '.$parent->title.'-'.$i,
              'parent_id' => $request->child_id,
            ]);
            }

            $child_categories = Category::where('parent_id', $request->child_id)->get();

            foreach ($child_categories as $child_category) {
                $html .= '<option value="'.$child_category->id.'">'.$child_category->title.'</option>';
            }
        }
        return response()->json(['html' => $html]);
    }
}
